import svg4everybody from 'svg4everybody';
// import $ from 'jquery';

$(() => {
	svg4everybody();

	$('.js-partners-carousel').owlCarousel({
		lazyLoad: true,
		margin: 30,
		dots: false,
		nav: false,
		items: 5,
		loop: true,
		autoplay: true,
		mouseDrag: false,
		touchDrag: false
	});
	$('.js-party-carousel').owlCarousel({
		lazyLoad: true,
		dots: false,
		nav: false,
		items: 1,
		loop: true,
		autoplay: true,
		mouseDrag: false,
		touchDrag: false
	});
	$('.js-super-banner-carousel').owlCarousel({
		lazyLoad: true,
		dots: false,
		nav: false,
		items: 1,
		loop: true,
		autoplay: true,
		mouseDrag: false,
		touchDrag: false,
		animateOut: 'fadeOut'
	});

	// Open sidebar cats on mouse hover
	var $sidebarCats = $('#simple-cats__toggler');
	$sidebarCats.on('mouseover', function () {
		var $this = $(this);
		setTimeout(function () {
			if (!$this.parent().hasClass('open')) {
				$this.dropdown('toggle');
			}
		});
	});
	// Init sidebar cats dropdown when it not inited
	if ($sidebarCats.parent().hasClass('not-inited')) {
		$sidebarCats.one('click', function () {
			$sidebarCats.attr('data-toggle', 'dropdown');
		});
	}
});
