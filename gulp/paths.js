export default {
	appImages: 'app/images',
	appStylesHelpers: 'app/styles/helpers',
	dist: 'dist',
	assets: 'dist/static/',
	images: 'dist/static/images',
	scripts: 'dist/static/scripts',
	styles: 'dist/static/styles'
};
